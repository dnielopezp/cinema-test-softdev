/* eslint-disable */

import React from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import UserScreen from '../components/UserScreen';
import Movies from '../components/Movies';
import Cinemas from '../components/Cinemas';

const Root = () => {
  return (
    <Router>
      <div>
        <div className="tabs">
          <div className="logo">CinemaTest</div>
          <div><Link href="/" to="/" >Cinema listings</Link></div>
          <div><Link href="/" to="/Movies" >CRUD Movies</Link></div>
          <div><Link href="/" to="/Cinemas" >CRUD Movie theaters</Link></div>
        </div>
        <Switch>
          <Route exact path="/" component={UserScreen} />
          <Route exact path="/Movies" component={Movies} />
          <Route exact path="/Cinemas" component={Cinemas} />
        </Switch>
      </div>

    </Router>
  );
};

export default Root;

