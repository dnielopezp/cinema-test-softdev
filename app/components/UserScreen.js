/* eslint-disable */

import React from 'react';

// Render function for cinemas list
/**
 *
 * @param {*} propss
 */
const Square = (propss) => {
  return (
    <button className={(propss.select === propss.id ? 'square select' : 'square')} onClick={() => {propss.func(propss.id)}} >
      {propss.cinema.name}
    </button>
  );
};

// Render function for item list
/**
 *
 * @param {*} propss
 */
const ItemList = (propss) => {
  const movie = propss.movie;
  const movies = propss.movies;
  for (let i = 0 ; i < movies.length; i++) {
    if (movies[i]._id === movie) {
      return (
        <div className="item-list">
          <p>
            Movie: {movies[i].name} <br />
            Release Date: {movies[i].releaseDate} <br />
            Idioms: {movies[i].idiom}
          </p>
        </div>
      );
    }
  }
  return null;
};

class UserScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectIndex: 1,
      cinemas: [],
      movies: []
    };
  }
  // Change cinema
  changeMovie = (index) => {
    this.setState({
      selectIndex: index,
    });
  };

  getAll () {
    let RC = this;
    fetch('http://localhost:3000/cinemas', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      }
    }).then((response) => {
      return response.json();
    })
    .then((responseJson) => {
      RC.setState({cinemas: responseJson.data});
    })
    .catch((response) => {
      // console.log(response);
    });
  };

  getMovies () {
    let RC = this;
    fetch('http://localhost:3000/movies', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      }
    }).then((response) => {
      return response.json();
    })
    .then((responseJson) => {
      RC.setState({movies: responseJson.data});
    })
    .catch((response) => {
      // console.log(response);
    });
  };

  componentDidMount () {
    this.getAll();
    this.getMovies();
  };

  render () {

    // List of cinemas
    const listCinemas = this.state.cinemas.map((cinema, key) => {
      const index = key + 1;
      return (
        <Square
          cinema={cinema}
          key={index}
          select={this.state.selectIndex}
          id={index}
          func={this.changeMovie}
        />
      );
    });

    // List of movies
    const listMovies = this.state.cinemas[this.state.selectIndex - 1] ?this.state.cinemas[this.state.selectIndex - 1].movies.map((movie, key) => {
      const index = key + 1;
      return (<ItemList movie={movie} movies={this.state.movies} key={index} />);
    }) : null;

    return (
      <div className="content-page">
        <h2 id="heading">Cinemas List </h2>
        <div className="cinemas-tabs">
          {listCinemas}
        </div>
        <h2 id="heading">Movies List</h2>
        <div>
          {listMovies}
        </div>
      </div>
    );
  }
}

export default UserScreen;
