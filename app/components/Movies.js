/* eslint-disable */

import React from 'react';

// Render function for item list
/**
 *
 * @param {*} propss
 */
const ItemList = (propss) => {
  const { name, releaseDate, idiom, _id } = propss.movie;
  const idiomMovie = idiom;
  return (
    <div className="item-list">
      <p>
        Movie: {name} <br />
        Release Date: {releaseDate} <br />
        Idiom: {idiomMovie}
      </p>
      <button className="btUpdate" onClick={() => {propss.func(propss.movie)}}>
        Update
      </button>
      <button className="btDelete" onClick={() => {propss.funcD(_id)}}>
        Delete
      </button>
    </div>
  );
};

class Movies extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showPopUp: false,
      movies: [],
      movieUpdate: {}
    };
  }

  showPop = (movie) => {
    document.getElementById('nameM').value = movie.name;
    document.getElementById('releaseM').value = movie.releaseDate;
    document.getElementById('idiomM').value = movie.idiom;
    this.setState({
      showPopUp: true,
      movieUpdate: movie
    });
  };

  createNew(event) {
    let RC = this;
    event.preventDefault();
    fetch('http://localhost:3000/movies', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        name: event.target.name.value,
        releaseDate: event.target.releaseDate.value,
        idiom: event.target.idiom.value
      })
    }).then((res) => {
      // console.log(response);
      if (res.ok) {
        document.getElementById('createname').value = '';
        document.getElementById('createrelease').value = '';
        document.getElementById('createidiom').value = '';
        RC.getAll();
      }
    }).catch((response) => {
      // console.log(response);
    });
  };

  getAll () {
    let RC = this;
    fetch('http://localhost:3000/movies', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      }
    }).then((response) => {
      return response.json();
    })
    .then((responseJson) => {
      RC.setState({movies: responseJson.data});
    })
    .catch((response) => {
      // console.log(response);
    });
  };

  deleteItem(id) {
    let RC = this;
    fetch('http://localhost:3000/movies', {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        'id': id
      })
    })
    .then((res) => {
      if (res.ok) {
        RC.getAll();
      }
    }).catch((response) => {
      // console.log(response);
    });
  };

  updateElement(event) {
    let RC = this;
    let nameV = document.getElementById('nameM').value;
    let releaseV = document.getElementById('releaseM').value;
    let idiomV = document.getElementById('idiomM').value;
    event.preventDefault();
    fetch('http://localhost:3000/movies', {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        'id': RC.state.movieUpdate._id,
        'name': nameV,
        'releaseDate': releaseV,
        'idiom': idiomV
      })
    })
    .then((res) => {
      if (res.ok) {
        RC.getAll();
        RC.setState({showPopUp: false});
      }
    }).catch((response) => {
      // console.log(response);
    });
  };

  componentDidMount () {
    this.getAll();
  };

  render () {

    // List of movies
    const listMovies = this.state.movies.map((movie, key) => {
      const index = key + 1;
      return (<ItemList movie={movie} key={index} func={this.showPop} funcD={this.deleteItem.bind(this)} />);
    });

    return (
      <div className="content-page">
        <h2 id="heading">CRUD Movies</h2>
        <h4>Create New</h4>
        <form className="formNewMovie" onSubmit={this.createNew.bind(this)}>
          <label>Fill with the information of the new Movie</label>
          <input id="createname" type="text" name="name" placeholder="Movie Name"  required />
          <input id="createrelease" type="text" name="releaseDate" placeholder="Release date" required />
          <input id="createidiom" type="text" name="idiom" placeholder="Idiom" required />
          <input className="btCreate" type="submit" value="Create" />
        </form>

        <hr />

        <h4>Movies List</h4>
        <div>
          {listMovies}
        </div>

        <div className={(this.state.showPopUp ? 'popUp show' : 'popUp')}>
          <form className="formUpdateMovie" onSubmit={this.updateElement.bind(this)}>
            <h4>Update Movie</h4>
            <br />
            <label>Fill with the information of the Movie</label>
            <br /><br />
            <input id="nameM" type="text" name="name" placeholder="Movie Name"  required />
            <input id="releaseM" type="text" name="releaseDate" placeholder="Release date" required />
            <input id="idiomM" type="text" name="idiom" placeholder="Idiom" required />
            <input className="btCreate" type="submit" value="Update" />
          </form>
          <button className="btClose" onClick={() => {this.setState({showPopUp: false})}}>(x) Close</button>
        </div>
      </div>
    );
  }
}

export default Movies;
