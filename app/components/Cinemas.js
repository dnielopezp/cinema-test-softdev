/* eslint-disable */

import React from 'react';

// Render function for item list
/**
 *
 * @param {*} propss
 */
const ItemList = (propss) => {
  const { name, location, _id, movies } = propss.cinema;
   // List of movies
   const listMovies = movies.map((movie, key) => {
    const index = key + 1;
    for (let i = 0 ; i < propss.movies.length; i++) {
      if (propss.movies[i]._id === movie) {
        return (
          <button className="movieItem" key={index}>
            {propss.movies[i].name}
          </button>
        );
      }
    }
    return null;
  });
  return (
    <div className="item-list">
      <p>
        Name: {name} <br />
        Location: {location} <br />
      </p>
      <div className="moviesList">
        {listMovies}
      </div>
      <button className="btUpdate" onClick={() => {propss.func(propss.cinema)}}>
        Update
      </button>
      <button className="btDelete"  onClick={() => {propss.funcD(_id)}}>
        Delete
      </button>
    </div>
  );
};

class Cinemas extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showPopUp: false,
      cinemas: [],
      movies: [],
      selecmovies: [],
      cinemaUpdate: {}
    };
  }

  showPop = (cinema) => {
    document.getElementById('nameM').value = cinema.name;
    document.getElementById('locationM').value = cinema.location;
    this.setState({
      showPopUp: true,
      cinemaUpdate: cinema
    });
  };

  createNew(event) {
    let RC = this;
    event.preventDefault();
    fetch('http://localhost:3000/cinemas', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        name: event.target.name.value,
        location: event.target.location.value,
        movies: RC.state.selecmovies
      })
    }).then((res) => {
      // if (res.ok) return res.json();
      if (res.ok) {
        RC.setState({selecmovies: []});
        document.getElementById('createname').value = '';
        document.getElementById('createlocation').value = '';
        RC.getAll();
      }
    })
    .catch((response) => {
      // console.log(response);
    });
  };

  getMovies () {
    let RC = this;
    fetch('http://localhost:3000/movies', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      }
    }).then((response) => {
      return response.json();
    })
    .then((responseJson) => {
      RC.setState({movies: responseJson.data});
    })
    .catch((response) => {
      // console.log(response);
    });
  };

  getAll () {
    let RC = this;
    fetch('http://localhost:3000/cinemas', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      }
    }).then((response) => {
      return response.json();
    })
    .then((responseJson) => {
      RC.setState({cinemas: responseJson.data});
    })
    .catch((response) => {
      // console.log(response);
    });
  };

  deleteItem(id) {
    let RC = this;
    fetch('http://localhost:3000/cinemas', {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        'id': id
      })
    })
    .then((res) => {
      if (res.ok) {
        RC.getAll();
      }
    })
    .catch((response) => {
      // console.log(response);
    });
  };

  selectMovie () {
    let RC = this, aux;
    let selector = document.getElementById('movieslist');
    for (let i=0; i < RC.state.movies.length; i++) {
      if (RC.state.movies[i]._id === selector.value) {
        aux = RC.state.selecmovies;
        aux.push(RC.state.movies[i]._id);
        RC.setState({
          selecmovies: aux
        });
        selector.value = '';
        return;
      }
    }
  };

  removeMovieSelect (id) {
    let RC = this, aux;
    aux = RC.state.selecmovies;
    aux.splice(id, 1);
    RC.setState({
      selecmovies: aux
    });
  };

  removeMovieSelectUpdate (id) {
    let RC = this, aux, arr = [];
    for (let i = 0; i < RC.state.cinemaUpdate.movies.length; i++) {
      arr[i] = RC.state.cinemaUpdate.movies[i];
    }
    arr.splice(id, 1)
    aux = {
      _id: RC.state.cinemaUpdate._id,
      name: RC.state.cinemaUpdate.name,
      location: RC.state.cinemaUpdate.location,
      movies: arr,
    };
    RC.setState({
      cinemaUpdate: aux
    });
  };

  updateElement(event) {
    let RC = this;
    let nameV = document.getElementById('nameM').value;
    let locationV = document.getElementById('locationM').value;
    let moviesV = RC.state.cinemaUpdate.movies;
    event.preventDefault();
    fetch('http://localhost:3000/cinemas', {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        'id': RC.state.cinemaUpdate._id,
        'name': nameV,
        'location': locationV,
        'movies': moviesV,
      })
    })
    .then((res) => {
      if (res.ok) {
        RC.getAll();
        RC.setState({showPopUp: false});
      }
    }).catch((response) => {
      // console.log(response);
    });
  };

  selectMovieUpdate () {
    let RC = this, aux = {movies: []};
    let selector = document.getElementById('selectorUpdate');
    for (let i=0; i < RC.state.movies.length; i++) {
      if (RC.state.movies[i]._id === selector.value) {
        aux = {
          _id: RC.state.cinemaUpdate._id,
          name: RC.state.cinemaUpdate.name,
          location: RC.state.cinemaUpdate.location,
          movies: [RC.state.movies[i]._id].concat(RC.state.cinemaUpdate.movies),
        };
        RC.setState({
          cinemaUpdate: aux
        });
        selector.value = '';
        return;
      }
    }
  };


  componentDidMount () {
    this.getAll();
    this.getMovies();
  };

  render () {

    // List of cinemas
    const listCinemas = this.state.cinemas.map((cinema, key) => {
      const index = key + 1;
      return (<ItemList movies={this.state.movies} cinema={cinema} key={index} func={this.showPop} funcD={this.deleteItem.bind(this)} />);
    });

    // List of movies
    const listMovies = this.state.movies.map((movie, key) => {
      const index = key + 1;
      return (<option value={movie._id} key={index}>{movie.name}</option>);
    });

    // List of select movies
    const selctmovi = this.state.selecmovies.map((movie, key) => {
      const index = key + 1;
      for (let i = 0 ; i < this.state.movies.length; i++) {
        if (this.state.movies[i]._id === movie) {
          return (
            <button className="movieItem" key={index} onClick={() => {this.removeMovieSelect(key)}}>
              {this.state.movies[i].name} (x)
            </button>
          );
        }
      }
      return null;
    });

    // List of select movies UPDATE
    const selctmoviUpdate = this.state.cinemaUpdate.movies ? this.state.cinemaUpdate.movies.map((movie, key) => {
      const index = key + 1;
      for (let i = 0 ; i < this.state.movies.length; i++) {
        if (this.state.movies[i]._id === movie) {
          return (
            <button type="button" className="movieItem" key={index} onClick={() => {this.removeMovieSelectUpdate(key)}}>
              {this.state.movies[i].name} (x)
            </button>
          );
        }
      }
      return null;
    }) : null;

    return (
      <div className="content-page">
        <h2 id="heading">CRUD Cinemas</h2>
        <h4>Create New</h4>
        <form className="formNewMovie" onSubmit={this.createNew.bind(this)}>
          <label>Fill with the information of the new Cinema</label>
          <input id="createname" type="text" name="name" placeholder="Cinema Name" required />
          <input id="createlocation" type="text" name="location" placeholder="Place" required />
          <select id="movieslist" name="movies" onChange={this.selectMovie.bind(this)}>
            <option value="">Select movie</option>
            {listMovies}
          </select>
          <input className="btCreate" type="submit" value="Create" />
        </form>
        <div className="moviesList">
          {selctmovi}
        </div>

        <hr />

        <h4>Cinema List</h4>
        <div>
          {listCinemas}
        </div>

        <div className={(this.state.showPopUp ? 'popUp show' : 'popUp')}>
          <form className="formUpdateMovie" onSubmit={this.updateElement.bind(this)}>
            <h4>Update Cinema</h4>
            <br />
            <label>Fill with the information of the Cinema</label>
            <br /><br />
            <input id="nameM" type="text" placeholder="Cinema Name" required />
            <input id="locationM" type="text" placeholder="Place" required />
            <select id="selectorUpdate" onChange={this.selectMovieUpdate.bind(this)}>
              <option value="" >Select Movie</option>
              {listMovies}
            </select>
            <input className="btCreate" type="submit" value="Update" />
            <div className="moviesList">
              {selctmoviUpdate}
            </div>
          </form>
          <button className="btClose" onClick={() => {this.setState({showPopUp: false})}}>(x) Close</button>
        </div>
      </div>
    );
  }
}

export default Cinemas;
