# Cinema Test for SoftDev

## Starting the dev server

Make sure you have the latest Stable or LTS version of Node.js installed.

1. Clone this repo in your localhost
2. Run `npm install` or `yarn install`
3. Open one tab in your terminal and run `cd dbMongo`, after using `npm run dev` to start server with node (Express)
3. Start the dev server using `npm start` in other tab in your terminal
4. Open [http://localhost:8080](http://localhost:8080)

## This project work with: 

- [x] React 16.2.0
- [x] ECMAScript 6 and JSX support
- [x] React Router v4
- [x] Latest Webpack (v.3.9.1) and Webpack Dev Server (v.2.9.5) with Scope Hoisting enabled
- [x] Hot Module Replacement using [react-hot-loader](https://github.com/gaearon/react-hot-loader)
- [x] SASS support
- [x] Nodejs
- [x] Express
- [x] MongoDB

## Author
Daniel López
dniel_lopez@hotmail.com
Bogota, Colombia
