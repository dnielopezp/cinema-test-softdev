const express = require('express');
const app = express();
const bodyParser= require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;

//Database mongodb
var db;

MongoClient.connect('mongodb://danieltest:passw0rd!@ds137336.mlab.com:37336/test-cinema-softdev', (err, database) => {
  if (err) return console.log(err);
  db = database.db('test-cinema-softdev');

  app.use(bodyParser.json());  

  //Listen method where browsers can connect with us
  app.listen(3000, () => {
    console.log('listening on 3000');
  });

  /**
   * MOVIES METHODS-----------------------------------
   */

  // READ Get all movies
  app.get('/movies', (req, res) => {
    db.collection('movies').find().toArray(function(err, results) {
      if (err) return res.send(false);
      res.json({data: results});
    });
  });

  // POST Create Movies
  app.post('/movies', (req, res) => {
    db.collection('movies').save(req.body, (err, result) => {
      if (err) return res.send(false);
      res.send(true);
    })
  });

  // DELETE Movie
  app.delete('/movies', (req, res) => {
    db.collection('movies').findOneAndDelete({_id: ObjectId(req.body.id)},
    (err, result) => {
      if (err) return res.send(500, err);
      res.send(true);
    });
  });

  // PUT Update Movie
  app.put('/movies', (req, res) => {
    db.collection('movies')
    .findOneAndUpdate({_id: ObjectId(req.body.id)}, {
      $set: {
        name: req.body.name,
        releaseDate: req.body.releaseDate,
        idiom: req.body.idiom
      }
    }, {}, (err, result) => {
      if (err) return res.send(err)
      res.send(result)
    })
  });

  /**
   * CINEMAS METHODS-----------------------------------
   */
  // READ Get all cinema
  app.get('/cinemas', (req, res) => {
    db.collection('cinemas').find().toArray(function(err, results) {
      if (err) return res.send(false);
      res.json({data: results});
    });
  });

  // POST Create cinema
  app.post('/cinemas', (req, res) => {
    db.collection('cinemas').save(req.body, (err, result) => {
      if (err) return res.send(false);
      res.json({data: result});
    })
  });

  // DELETE cinema
  app.delete('/cinemas', (req, res) => {
    db.collection('cinemas').findOneAndDelete({_id: ObjectId(req.body.id)},
    (err, result) => {
      if (err) return res.send(500, err);
      res.send(true);
    });
  });

  // PUT Update cinemas
  app.put('/cinemas', (req, res) => {
    db.collection('cinemas')
    .findOneAndUpdate({_id: ObjectId(req.body.id)}, {
      $set: {
        name: req.body.name,
        location: req.body.location,
        movies: req.body.movies,
      }
    }, {}, (err, result) => {
      if (err) return res.send(err)
      res.send(result)
    })
  });
});
